# ducky

JS type coercion using the "duck" method

## Usage

### Import

```js
const { coerceBool, coerceNumber, coerceValue } = require('ducky');
```

### Use it

```js
const obj = {
    bool: coerceBool(1),
    num: coerceNumber('1234'),
    val1: coerceValue('4321'),
    val2: coerceValue('false')
};
```

### Result

```json
{
    "bool": true,
    "num": 1234,
    "val1": 4321,
    "val2": false
}
```
