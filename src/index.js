const {
    F,
    T,
    both,
    complement,
    cond,
    construct,
    either,
    equals,
    identity,
    is,
    pipe,
    toLower
} = require('ramda');

const stringIsTruthy = both(
    is(String),
    either(x => toLower(x) === 'true', x => x === '1')
);

const stringIsFalsy = both(
    is(String),
    either(x => toLower(x) === 'false', x => x === '0')
);

const stringIsBool = both(is(String), either(stringIsFalsy, stringIsTruthy));

const numberIsNaN = both(is(Number), isNaN);

const stringIsNumber = both(
    is(String),
    pipe(
        construct(Number),
        complement(numberIsNaN)
    )
);

const coerceBool = cond([
    [equals(true), T],
    [equals(false), F],
    [stringIsTruthy, T],
    [stringIsFalsy, F],
    [
        T,
        () => {
            throw new Error('this value cannot be coerced to a Boolean');
        }
    ]
]);

const coerceNumber = cond([
    [is(Number), identity],
    [stringIsNumber, construct(Number)],
    [
        T,
        () => {
            throw new Error('this value cannot be coerced to a Number');
        }
    ]
]);

const coerceValue = cond([
    [stringIsNumber, coerceNumber],
    [stringIsBool, coerceBool],
    [T, identity]
]);

const duck = {
    stringIsTruthy,
    stringIsFalsy,
    stringIsBool,
    stringIsNumber,
    numberIsNaN,
    coerceBool,
    coerceNumber,
    coerceValue
};

module.exports = exports = duck;
