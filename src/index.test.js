const {
    stringIsTruthy,
    stringIsFalsy,
    stringIsBool,
    stringIsNumber,
    numberIsNaN,
    numberIsNotNaN,
    coerceBool,
    coerceNumber,
    coerceValue
} = require('./index.js');

describe('duck', () => {
    test('stringIsTruthy', () => {
        expect(stringIsTruthy('true')).toEqual(true);
        expect(stringIsTruthy('1')).toEqual(true);
        expect(stringIsTruthy('false')).toEqual(false);
        expect(stringIsTruthy('0')).toEqual(false);
        expect(stringIsTruthy('asdf')).toEqual(false);
    });

    test('stringIsFalsy', () => {
        expect(stringIsFalsy('true')).toEqual(false);
        expect(stringIsFalsy('1')).toEqual(false);
        expect(stringIsFalsy('false')).toEqual(true);
        expect(stringIsFalsy('0')).toEqual(true);
        expect(stringIsFalsy('asdf')).toEqual(false);
    });

    test('stringIsBool', () => {
        expect(stringIsBool('true')).toEqual(true);
        expect(stringIsBool('false')).toEqual(true);
        expect(stringIsBool('0')).toEqual(true);
        expect(stringIsBool('1')).toEqual(true);
        expect(stringIsBool('asdf')).toEqual(false);
        expect(stringIsBool('something')).toEqual(false);
    });

    test('numberIsNaN', () => {
        expect(numberIsNaN(0)).toEqual(false);
        expect(numberIsNaN(1)).toEqual(false);
        expect(numberIsNaN(NaN)).toEqual(true);
    });

    test('stringIsNumber', () => {
        expect(stringIsNumber('12345')).toEqual(true);
        expect(stringIsNumber('54321')).toEqual(true);
        expect(stringIsNumber('0')).toEqual(true);
        expect(stringIsNumber('1')).toEqual(true);
        expect(stringIsNumber('asdf')).toEqual(false);
        expect(stringIsNumber('something')).toEqual(false);
    });

    test('coerceBool', () => {
        expect(coerceBool('true')).toEqual(true);
        expect(coerceBool('1')).toEqual(true);
        expect(coerceBool('false')).toEqual(false);
        expect(coerceBool('0')).toEqual(false);
        expect(coerceBool(true)).toEqual(true);
        expect(coerceBool(false)).toEqual(false);
        expect(() => coerceBool('something')).toThrow();
        expect(() => coerceBool(-1)).toThrow();
    });

    test('coerceNumber', () => {
        expect(coerceNumber('0')).toEqual(0);
        expect(coerceNumber('1')).toEqual(1);
        expect(coerceNumber(-1)).toEqual(-1);
        expect(() => coerceNumber('something')).toThrow();
        expect(() => coerceNumber(true)).toThrow();
        expect(() => coerceNumber(false)).toThrow();
    });

    test('coerceValue', () => {
        expect(coerceValue('true')).toEqual(true);
        expect(coerceValue('1')).toEqual(1);
        expect(coerceValue('false')).toEqual(false);
        expect(coerceValue('0')).toEqual(0);
        expect(coerceValue(true)).toEqual(true);
        expect(coerceValue(false)).toEqual(false);
        expect(coerceValue('something')).toEqual('something');
        expect(coerceValue(-1)).toEqual(-1);
    });
});
